# Using Android App Bundles with Approov

Google is rolling out a new application packaging format for applications. The [Android Application Bundle (AAB)](https://developer.android.com/guide/app-bundle/) includes platform, screen density, and langauge variants needed for application installation on any device. After a bundle has been uploaded to the playstore, each device installation will include only the specific platform, density, and language features needed by that device. The installation is delivered using Android's split-APKs feature, and post-installation dynamic feature modules can be provided on demand using supplemental APKs.

This repository describes how to use Android app bundles with current Approov app authentication. It includes additions to gradle tooling and a simple client app demonstrating a bundle-compatible flow. The example app's gradle files include tasks for installing and registering local APKs and app bundles, and can be adapted to your own build environment. 

The example app uses the on-line [Approov demo backend server](https://demo-server.approovr.io/hello), so if you wish to recreate this example, please carefully follow the setup instructions below.

### Approov Support for Application Bundles

The new bundle format and installation process complicates the Approov attestation process. Full support will be coming in future Approov releases.

An app's Android app bundle generates a range of split-APKs including a master base APK (base-master.apk), a set of base configuration APKs (split across different platforms, densities, and languages), and a set of dynamic feature APKs (similarly split into master, platform, density, and language APKs for each feature).

![Split APK Tree](./README.assets/SplitAPKs.png)

During app installation, the `base-master.apk` is the one APK which will always be downloaded for any device supporting split-APKs (Android 5.0 and above). Your application registers this `base-master.apk` with the Approov service for attestation. All other APKs associated with your application share the same application ID and signing configuration with the registered base mater APK.

### Limitations and Additional Development

Approov AAB support will improve over time. The following current limitations should be noted:

- Devices running Android 4.4 (API level 19) and lower do not support split-APKs. Android app bundle support for pre Android 5.0 requires a special multi-APK variant. Pre 5.0 devices are not yet supported in this current tooling.

- Registering playstore bundles with Approov currently requires a manual download (described below). Automating this download requires some additional investigation and scripting support not yet in place.

- This initial release has been lightly tested on Mac OS and Windows. Additional in-depth testing is still needed.

## Example Setup

Follow these set up instructions to prepare your environment to work the example client app.

### Prepare the Approov AAB Usage environment

First ensure that a Java JDK is installed. Set `JAVA_HOME` to point to the JDK's top directory. Add the JDK's `bin` directory to your `PATH`. 

Also ensure that Android Studio, version 3.2 or later, is installed.

If you haven't already, extract the `approov-aab-usage` archive which contains this README.

Next, create `vault` and `tools` directories inside the `approov-aab-usage` directory:

```
$ cd approov-aab-usage
$ mkdir vault tools
```

### Install the Android Bundletool

Google provides an open-source `bundletool` to manage app bundles. You can learn more about the bundletoool at [https://developer.android.com/studio/command-line/bundletool](https://developer.android.com/studio/command-line/bundletool). The bundletool project is located at [https://github.com/google/bundletool](https://github.com/google/bundletool).

First, make a new directory, `bundletool`, inside the `tools` directory. Next, download the [latest bundletool release](https://github.com/google/bundletool/releases) into the `bundletool` directory.

### Create the Release keystore

Debug AAB bundles are signed by the same keystore as debug APKs. Likewise, release AABs use the same signing configuration as release APKs. Cretae a new `release.keystore` inside the `vault` directory. You can do this within Android studio, or directly from the command line:

```
$ cd vault
$ keytool -genkey -alias signing \
    -keyalg RSA -keystore keystore.jks \
    -dname "CN=Happy Customer, OU=Approov" \
    -storepass store-password -keypass signing-password
```

### Download Approov demo package

Our example client app uses the [Approov demo backend server](https://demo-server.approovr.io/hello). The Approov demo download package is required to properly acces this server. Request the demo package from [https://info.approov.io/demo](https://info.approov.io/demo), and select the `Shapes Hands-On Demo` when completing the form:

![Demo Request Form](./README.assets/DemoRequest.png)

Follow the instructions in the subsequent email.

First, copy the registration token string into a text file, `reg-access.tok`, and save that file inside the `vault` directory.

Second, download and unip the Approov demo package. Locate the registration tools inside the demo package at `approov-demo/registration-tools`. Copy the `registration-tools` into your `tools` directory.

### Add Approov SDK into the Client App

The example client app requires the Approov demo SDK in order to properly attest the running app.

Locate the Approov demo SDK inside the downloaded Approov demo package at `approov-demo/libraries/android/approov.aar`. Copy the AAR library into the Approov module at `client-app/approov/approov.aar`.

### Create the Configuration file

Copy the sample configuration settings file, `client-app/config.sample.gradle` into `client-app/config.gradle`. The sample file looks like this:

```
ext {
    homePath = System.env.HOME
    vaultPath = "${rootProject.projectDir}/../vault"
    toolsPath = "${rootProject.projectDir}/../tools"

    config = [
        applicationId:        "com.example.aabclient.x1",
        debug: [
            keystorePath:     "${homePath}/.android/debug.keystore",
            keystorePassword: "android",
            keyAlias:         "androiddebugkey",
            keyPassword:      "android"
        ],
        release: [
            keystorePath:     "${vaultPath}/release.keystore",
            keystorePassword: "store-password",
            keyAlias:         "signing",
            keyPassword:      "signing-password"
        ],
        approov: [
            regToolsPath:     "${toolsPath}/registration-tools",
            regTokenPath:     "${vaultPath}/reg-access.tok",
            regExpiration:    "1h"
        ],
        android: [
            buildPlatform:    System.getProperty('os.name'),
            bundletoolPath:   "${toolsPath}/bundletool/bundletool-all-0.7.2.jar"
        ]
    ]
}
```

If you followed the set up instructions, the default sample configuration should work properly for local testing.

If you want to upload your example into the playstore, you may need to change the `applicationId` to a unique value.

The debug keystore information points to the default debug keystore installed by Android. You may use any other keystores by specifying different paths. 

The 'regExpiration' time is set to `1h` (1 hour) for testing purposes. This applies to debug and release registrations. By default, playstore registrations will be set to never expire, although the demo Approov service will override this limit to 30 days.

You may need to change the version of the bundletool's JAR to match the version you downloaded.

### Gradle Modifications

The `config.gradle` file discussed above was added to localize configuration set up. 

Tasks used to install app bundles and register the app bundles with Approov are all located in the `approov.gradle` file:

```
task registerDebug() { ... }
task registerRelease() { ... }
task installDebugBundle() { ... }
task installReleaseBundle() { ... }
task registerDebugBundle() { ... }
task registerReleaseBundle() { ... }
task registerPlaystoreBundle() { ... }
```


The only change made to the client app's project `build.gradle` file is to `apply` the `config.gradle` file:

```
// Top-level build file where you can add configuration options common to all sub-projects/modules.

apply from: "config.gradle"

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.2.1'
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
```

The app module's `app/build.gradle` file includes the usual modifications for signing configuration and implementation dependencies. Beyond this, the only addition made to the client app's project `app/build.gradle` file is to `apply` the `approov.gradle` file:

```
apply plugin: 'com.android.application'

apply from: "${rootProject.projectDir}/approov.gradle"

android {
    defaultConfig { ... }
    signingConfigs {
        release { ...}
    }
    buildTypes {
        release { ...}
    }
}

dependencies { ...
}
```

You can use this approach as a starting point for your own app bundle build scripts.

### Final Set up

Your final file set up should look like this:

![Project Configuration](./README.assets/ConfigFiles.png)

## Development Scenarios

The following are a list of useful scenarios when working with Android app bundles and Approov. 

The scenarios are mostly run from the command line, for example, in the `Terminal` window inside Android Studio.

### List Added Tasks

To see useful gradle tasks, in the `client-app` directory, run: 

```
$ ./gradlew tasks
```

This includes the following relevant app bundle and Approov tasks:

```
Approov tasks
-------------
registerDebug - Registers the Debug APK with the Approov service
registerDebugBundle - Registers the Debug bundle with the Approov service
registerPlaystoreBundle - Registers the Playstore bundle with the Approov service
registerRelease - Registers the Release APK with the Approov service
registerReleaseBundle - Registers the Release bundle with the Approov service

Build tasks
-----------
assembleDebug - Assembles all Debug builds.
assembleRelease - Assembles all Release builds.
bundleDebug - Creates all Debug bundles.
bundleRelease - Creates all Release bundles.
clean - Deletes the build directory.

Install tasks
-------------
installDebug - Installs the Debug build.
installDebugBundle - Installs the Debug bundle
installRelease - Installs the Release build.
installReleaseBundle - Installs the Release bundle
```

### Check Signing Config

It's often useful to check the signing configurations being used. In the `client-app` directory, run: 

```
$ ./gradlew signingReport
```

Configuration reports will be similar to:

```
Variant: debug
Config: debug
Store: /Users/skiph/.android/debug.keystore
Alias: AndroidDebugKey
MD5: 67:FE:75:B8:12:5F:E8:DA:BF:76:9B:AD:5A:0C:8D:F3
SHA1: 59:99:5C:30:63:09:AE:A8:98:F6:DC:9F:23:77:F9:05:43:E2:CB:CA
Valid until: Tuesday, October 13, 2048
----------
Variant: release
Config: release
Store: /Users/skiph/projects/approov-aab/approov-aab-usage/vault/release.keystore
Alias: signing
MD5: 23:E9:BD:2A:1D:57:50:A9:D3:84:8F:4D:6B:C3:2E:C0
SHA1: 18:CA:79:4A:B6:91:B3:99:D7:9E:85:D0:78:EC:3D:6C:1C:31:8C:6A
Valid until: Tuesday, April 2, 2019
```

Note that in this setup, the release signing key will expire in 2019!.

### Assemble and Run Example Client as Debug/Release APK

Test the example client app by building and running a normal single APK package. Assemble, install, and launch the app:

```
$ ./gradlew assembleDebug
$ ./gradlew installDebug
$ adb shell am start -n com.example.aabclient.x1/com.example.aabclient.MainActivity
Starting: Intent { cmp=com.example.aabclient.x1/com.example.aabclient.MainActivity }
```

Note that `com.example.aabclient.x1` is the sample application Id. If you change it, you will need to adjust the adb start command accordingly.

You should see an initial start up screen:

![Splash screen](./README.assets/AppSplash.png)

Pressing the `Say Hello` button checks the network connectivity and should return:

![Valid network check](./README.assets/AppHelloGood.png)

Pressing the `Get Shape` button checks the App authenticity through Approov and should return a failure if the app has not been registered:

![Invalid app auth](./README.assets/AppAuthBad.png)

### Register Debug/Release APK with Approov

Once assembled and installed, the APK will fail Approov authentication checks until the APK has been registered with the Approov service.

```
$ ./gradlew assembleDebug
$ ./gradlew installDebug
$ ./gradlew registerDebug

> Task :app:registerDebug 
/Users/skiph/projects/approov-aab/approov-aab-usage/tools/registration-tools/Android/Mac/registration -t /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/../vault/reg-access.tok -a /Users/ski
ph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/apk/debug/app-debug.apk -e 2m
Submitting data...
Success: new app signature added to database.
You can check in the Approov portal that signature R6GT7pl93/km0MjqNE+KeLnJjzEv6WUe/aKFWfsRyqY= has been added to the library 1534517906147482650

$ adb shell am start -n com.example.aabclient.x1/com.example.aabclient.MainActivity
Starting: Intent { cmp=com.example.aabclient.x1/com.example.aabclient.MainActivity }
```

The app is reinstalled above to ensure any existing invalid tokens are cleared from previous installations. Without reinstallation, any invalid tokens may take up to five minutes to clear.

Now that the app has been registered, pressing the `Get Shape` button should pass the Approov app authenticity check:

![Valid app auth](./README.assets/AppAuthRed.png)

### Assemble Debug/Release App Bundle

Android Studio 3.2 includes gradle tasks for assembling app bundles. These may be built using the Android Studio menus or the command line. Signing configurations for the debug and release build variants will be applied to the Android application bundle.

From the command line, run:

```
$ ./gradlew bundleDebug
```

The app bundles can be found in the `app` module build tree:

![Bundle files](./README.assets/BundleFiles.png)

### Install Debug/Release App Bundle

A custom task is provided to install app bundles onto a device. The task uses the bundletool to build and sign the APKs from the app bundle.

Assuming only one device is running and accessible from the build platform, the task installs the specific APKs needed by the device.

From the command line, run:

```
$ ./gradlew installDebugBundle
```

Upon success, the device will show the example `AAB Client` in the launcher:

![App Launcher](./README.assets/AppLaunch.png)

As with the usual single-APK installation, the app can be launched as normal:

```
$ adb shell am start -n com.example.aabclient.x1/com.example.aabclient.MainActivity
Starting: Intent { cmp=com.example.aabclient.x1/com.example.aabclient.MainActivity }
```

### Register Debug/Release App Bundle with Approov

A custom task is provided to register app bundles with the Approov service.

From the command line, run:

```
$ ./gradlew registerDebugBundle

> Task :app:registerDebugBundle 
unzip -j /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/debug/app.apks splits/base-master.apk -d /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/debug/
Archive:  /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/debug/app.apks
 extracting: /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/debug/base-master.apk
/Users/skiph/projects/approov-aab/approov-aab-usage/tools/registration-tools/Android/Mac/registration -t /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/../vault/reg-access.tok -a /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/debug/base-master.apk -e 2m
Submitting data...
Success: new app signature added to database.
You can check in the Approov portal that signature 6N4WwkXPqN6fzg+RUvITFFpXdH6vUGcYGjowUDZvzaA= has been added to the library 1534517906147482650
```

If necessary, launch the registered app:

```
$ adb shell am start -n com.example.aabclient.x1/com.example.aabclient.MainActivity
Starting: Intent { cmp=com.example.aabclient.x1/com.example.aabclient.MainActivity }
```

Since the app has been registered, pressing the `Get Shape` button should pass the Approov app authenticity check:

![Valid app auth](./README.assets/AppAuthRed.png)

If the app has previously failed authentication, it may take up to five mimutes to clear the failed check. Alternatively, 
reinstall the app to start with a fresh authentication check:

```
$ ./gradlew installDebugBundle
$ adb shell am start -n com.example.aabclient.x1/com.example.aabclient.MainActivity
Starting: Intent { cmp=com.example.aabclient.x1/com.example.aabclient.MainActivity }
```

### Submit App Bundle to Playstore

Once you are satisfied, submit your release app bundle to the Android playstore. The signing key in your release configuration should be used as the upload key for the playstore. See [https://developer.android.com/studio/publish/upload-bundle](https://developer.android.com/studio/publish/upload-bundle) for assistance.

### Register Playstore App Bundle

Currently, registering a playstore app bundle with Approov requires a manual step.

Start by logging into the [Playstore console](https://play.google.com/apps/publish/). Select the application which you uploaded the app bundle to, and select `Release Management > Artifact Library` from the left-hand menu:

![ArtifactLibrary](./README.assets/ArtifactLib.png)

Next, select the `EXPLORE` button to see a list of split-apk bundles per configuration:

![BundleExplorer](./README.assets/BundleExplorer.png)

Expand the `Download device-specific APKs` section using the `v` pulldown to see a long list of devices:

![Device APKs](./README.assets/DeviceAPKs.png)

In the rightmost column, download any one of the supported device APK archives, for example, the `Acer Iconia One 10 acer_jetfirelte` device. Extract the `base.apk` from the downloaded archive.

Create a `playstore` directory inside tne `app` module's build bundle directory, and copy the `base.apk` file into the client app project's `app` module as `app/build/outputs/bundle/playstore/base.apk`.

From the command line, run:

```
$ ./gradlew registerPlaystoreBundle

> Task :app:registerPlaystoreBundle 

WARNING: Registering a playstore bundle requires manual intervention.
WARNING: 
WARNING: 1) Download the Playstore's base.apk.
WARNING: 2) Copy it into /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/playstore/.
WARNING: 3) Reissue this registerPlaystoreBundle task.
WARNING: 4) Erase the /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/playstore/base.apk.
WARNING: 
WARNING: This registration has no expiration set.

/Users/skiph/projects/approov-aab/approov-aab-usage/tools/registration-tools/Android/Mac/registration -t /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/../vault/reg-access.tok -a /Users/skiph/projects/approov-aab/approov-aab-usage/client-app/app/build/outputs/bundle/playstore/base.apk -e never
This is the Approov demo. Your app registration will be valid for 30 days. You can re-register the app later if you'd like to continue playing with it!

Submitting data...
Success: new app signature added to database.
You can check in the Approov portal that signature 6N4WwkXPqN6fzg+RUvITFFpXdH6vUGcYGjowUDZvzaA= has been added to the library 1534517906147482650
```

Note that the playstore bundle registration will not use the registration expiration value specified in the `config.gradle file`. Normally, the playstore bundle registration will never expire, but since the example uses the Approov demo service, the expiration defaults to 30 days.

Once registered, install and run your application from the playstore. Launch the app, and pressing the `Get Shape` button should SUCCESSFULLY pass the Approov app authenticity check:

![Valid app auth](./README.assets/AppAuthRed.png)

## Support

For support, submit a request at [https://approov.zendesk.com/hc/en-gb/requests/new](https://approov.zendesk.com/hc/en-gb/requests/new) detailing any questions or suggested improvements.

---