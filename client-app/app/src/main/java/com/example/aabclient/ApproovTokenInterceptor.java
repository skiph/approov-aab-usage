package com.example.aabclient;

import android.util.Log;

import com.criticalblue.attestationlibrary.ApproovAttestation;
import com.criticalblue.attestationlibrary.TokenInterface;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApproovTokenInterceptor implements Interceptor {
    final static String TAG = ApproovTokenInterceptor.class.getSimpleName();

    final static String HEADER_NAME = "Approov-Token";

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request originalRequest = chain.request();

        Log.d(TAG, "starting fetch");
        // fetch an approov token synchronously
        TokenInterface.ApproovResults approovResults = ApproovAttestation.shared()
                .fetchApproovTokenAndWait(null);
        Log.d(TAG, "finishing fetch");

        // check return status
        String token = "UNSPECIFIED";
        if (approovResults.getResult() == ApproovAttestation.AttestationResult.SUCCESS) {
            Log.i(TAG, "Approov SDK token fetch succeeded");
            token = approovResults.getToken();
        } else {
            // no token returned
            Log.w(TAG, "Approov SDK token fetch failed");
            token = "NOTOKEN";
        }
        Log.i(TAG, token);

        // add approov token header
        Request approovTokenRequest = originalRequest.newBuilder()
                .header(HEADER_NAME, token)
                .build();

        return chain.proceed(approovTokenRequest);
    }
}

// end of file
