package com.example.aabclient;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private ShapesApp shapesApp;
    private Activity activity;

    private View statusView = null;
    private ImageView statusImageView = null;
    private TextView statusTextView = null;
    private Button connectivityCheckButton = null;
    private Button attestationCheckButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get app context

        shapesApp = ShapesApp.getInstance();
        activity = MainActivity.this;

        // find controls

        statusView = findViewById(R.id.viewStatus);
        statusImageView = (ImageView) findViewById(R.id.imgStatus);
        statusTextView = findViewById(R.id.txtStatus);
        connectivityCheckButton = findViewById(R.id.btnConnectionCheck);
        attestationCheckButton = findViewById(R.id.btnAttestationCheck);

        // handle connection check

        connectivityCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // hide status
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        statusView.setVisibility(View.INVISIBLE);
                    }
                });

                // use unprotected client
                OkHttpClient client = shapesApp.getHttpClient();

                // Initialize a new Request
                Request request = new Request.Builder()
                        .url(getResources().getString(R.string.hello_url))
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        // Do something when request failed
                        e.printStackTrace();
                        Log.d(TAG, "Request Failed.");
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        final int imgId;
                        final String msg;

                        if(response.isSuccessful()){
                            Log.d(TAG,"Connectivity check successful.");
                            imgId = R.drawable.hello;
                            msg = "" + response.code() + ": " + response.message();
                        } else {
                            Log.d(TAG,"Connectivity check unsuccessful.");
                            imgId = R.drawable.confused;
                            msg = "" + response.code() + ": " + response.message();
                        }

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                statusImageView.setImageResource(imgId);
                                statusTextView.setText(msg);
                                statusView.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });
            }
        });

        // handle attestation check

        attestationCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // hide status
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        statusView.setVisibility(View.INVISIBLE);
                    }
                });

                // use protected client
                OkHttpClient client = shapesApp.getApproovHttpClient();

                // Initialize a new Request
                Request request = new Request.Builder()
                        .url(getResources().getString(R.string.shapes_url))
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        // Do something when request failed
                        e.printStackTrace();
                        Log.d(TAG, "Request Failed.");
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        final int imgId;
                        final String msg;

                        if(response.isSuccessful()){
                            Log.d(TAG,"Attestation check successful.");
                            String shape = response.body().string().toLowerCase();
                            if (shape.equalsIgnoreCase("square")) {
                                imgId = R.drawable.square;
                            } else if (shape.equalsIgnoreCase("circle")) {
                                imgId = R.drawable.circle;
                            } else if (shape.equalsIgnoreCase("rectangle")) {
                                imgId = R.drawable.rectangle;
                            } else if (shape.equalsIgnoreCase("triangle")) {
                                imgId = R.drawable.triangle;
                            } else {
                                imgId = R.drawable.confused;
                            }
                            msg = "" + response.code() + ": " + response.message();
                        } else {
                            Log.d(TAG,"Attestation check unsuccessful.");
                            imgId = R.drawable.confused;
                            msg = "" + response.code() + ": " + response.message();
                        }

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                statusImageView.setImageResource(imgId);
                                statusTextView.setText(msg);
                                statusView.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });
            }
        });
    }
}

// end of file
