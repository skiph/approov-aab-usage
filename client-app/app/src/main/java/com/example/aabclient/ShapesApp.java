package com.example.aabclient;

import android.app.Application;
import android.util.Log;

import com.criticalblue.attestationlibrary.ApproovAttestation;
import com.criticalblue.attestationlibrary.ApproovConfig;

import java.net.MalformedURLException;

import okhttp3.OkHttpClient;

public class ShapesApp extends Application {
    private static final String TAG = ShapesApp.class.getSimpleName();

    // static singleton access

    private static ShapesApp instance = null;

    public static ShapesApp getInstance() {
        return instance;
    }

    // the singleton instance description

    private OkHttpClient httpClient;
    private OkHttpClient approovHttpClient;

    @Override
    public void onCreate(){
        super.onCreate();
        instance = this;

        // initialize the Approov SDK
        try {
            ApproovConfig config = ApproovConfig.getDefaultConfig(this);
            ApproovAttestation.initialize(config);
        } catch (IllegalArgumentException | MalformedURLException ex) {
            Log.e(TAG, ex.getMessage());
        }

        // unprotected http client
        httpClient = new OkHttpClient();

        Log.i(TAG, "Created new httpclient");

        // Approov-protected http client
        approovHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new ApproovTokenInterceptor())
                .build();
    }

    // Returns a hundle for the Approov attestation object
    public ApproovAttestation getApproovAttestation(){
        return ApproovAttestation.shared();
    }

    public OkHttpClient getHttpClient(){
        return httpClient;
    }

    public OkHttpClient getApproovHttpClient(){
        return approovHttpClient;
    }
}

// end of file
